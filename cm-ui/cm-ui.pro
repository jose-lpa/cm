include(../qmake-target-platform.pri)
include(../qmake-destination-path.pri)

QT += qml quick xml

TEMPLATE = app

CONFIG += c++14

INCLUDEPATH += source \
    ../cm-lib/source

SOURCES += source/main.cpp

RESOURCES += views.qrc \
    assets.qrc \
    components.qrc

# Additional import path used to resolve QML modules in Qt Creator code model
QML_IMPORT_PATH += $$PWD

LIBS += -L$$PWD/../binaries/$$DESTINATION_PATH -lcm-lib

message(cm-ui project dir: $${PWD})

DESTDIR = $$PWD/../binaries/$$DESTINATION_PATH
OBJECTS_DIR = $$PWD/build/$$DESTINATION_PATH/.obj
MOC_DIR = $$PWD/build/$$DESTINATION_PATH/.moc
RCC_DIR = $$PWD/build/$$DESTINATION_PATH/.qrc
UI_DIR = $$PWD/build/$$DESTINATION_PATH/.ui

DISTFILES += \
    components/RssItemDelegate.qml \
    views/RssView.qml
