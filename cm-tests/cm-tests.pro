include(../qmake-target-platform.pri)
include(../qmake-destination-path.pri)

QT += testlib
QT -= gui

TARGET = client-tests
TEMPLATE = app

CONFIG += c++14
CONFIG += qt console warn_on depend_includepath testcase
CONFIG -= app_bundle

INCLUDEPATH += \
    source \
    ../cm-lib/source

SOURCES +=  source/models/client-tests.cpp \
    data/datetime-decorator-tests.cpp \
    data/enumerator-decorator-tests.cpp \
    data/int-decorator-tests.cpp \
    data/string-decorator-tests.cpp \
    source/controllers/master-controller-tests.cpp \
    source/main.cpp \
    source/test-suite.cpp

LIBS += -L$$PWD/../binaries/$$DESTINATION_PATH -lcm-lib

DESTDIR = $$PWD/../binaries/$$DESTINATION_PATH
OBJECTS_DIR = $$PWD/build/$$DESTINATION_PATH/.obj
MOC_DIR = $$PWD/build/$$DESTINATION_PATH/.moc
RCC_DIR = $$PWD/build/$$DESTINATION_PATH/.qrc
UI_DIR = $$PWD/build/$$DESTINATION_PATH/.ui

HEADERS += \
    data/datetime-decorator-tests.h \
    data/enumerator-decorator-tests.h \
    data/int-decorator-tests.h \
    data/string-decorator-tests.h \
    source/controllers/master-controller-tests.h \
    source/models/client-tests.h \
    source/test-suite.h
