#include "master-controller-tests.h"

namespace cm {
namespace controllers {  // Instance

static MasterControllerTests instance;

MasterControllerTests::MasterControllerTests() : TestSuite("MasterControllerTests")
{
}

}  // Instance.

namespace controllers {  // Scaffolding.

void MasterControllerTests::initTestsCase()
{
}

void MasterControllerTests::cleanupTestCase()
{
}

void MasterControllerTests::init()
{
}

void MasterControllerTests::cleanup()
{
}

}  // Scaffolding.

namespace controllers {  // Tests.

void MasterControllerTests::welcomeMessage_returnsCorrectMessage()
{
    QCOMPARE(masterController.welcomeMessage(),
             QString("Welcome to the Client Management system!"));
}

}  // Tests.
}
